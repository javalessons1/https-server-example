package org.example.app.exception;

public class ApartmentNotFoundException extends RuntimeException {
    public ApartmentNotFoundException() {
    }

    public ApartmentNotFoundException(String message) {
        super(message);
    }
}
