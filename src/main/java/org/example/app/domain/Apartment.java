package org.example.app.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Apartment {
    private long id;
    private String roomAmount;
    private int price;
    private int size;
    private boolean withLoggia;
    private boolean withBalcony;
    private int floor;
    private int floorsInHouse;
    private String owner;
    private long created;
}
