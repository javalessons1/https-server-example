package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.ApartmentRQ;
import org.example.app.handler.ApartmentHandler;
import org.example.app.manager.AvitoManager;
import org.example.framework.auth.CertificatePrincipal;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.SecretAuthNMiddleware;
import org.example.framework.middleware.X509AuthNMiddleware;
import org.example.framework.util.Maps;

import java.io.IOException;
import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");
        System.setProperty("javax.net.ssl.trustStore", "web-certs/truststore.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "passphrase");
        final Gson gson = new Gson();
        final AvitoManager manager = new AvitoManager();
        final ApartmentHandler apartmentHandler = new ApartmentHandler(gson, manager);
        manager.create(new ApartmentRQ(
                "three rooms",
                6_000_000,
                84_5,
                true,
                true,
                10,
                10), new CertificatePrincipal("petya"));

        manager.create(new ApartmentRQ(
                "one room",
                2_600_000,
                38_5,
                true,
                false,
                3,
                9), new CertificatePrincipal("vasya"));

        final String getById = "^/apartments/(?<apartmentId>[0-9]{1,9})$";
        final String update = "^/apartments/(?<apartmentId>[0-9]{1,9})/update$";
        final String delete = "^/apartments/(?<apartmentId>[0-9]{1,9})/delete$";
        final Server server = Server.builder()
                .middleware(new X509AuthNMiddleware())
                .middleware(new SecretAuthNMiddleware())
                .middleware(new AnonAuthMiddleware())
                .routes(

                        Maps.of(
                                Pattern.compile("^/apartments$"), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getAll,
                                        HttpMethods.POST, apartmentHandler::create
                                ),
                                Pattern.compile(getById), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getById
                                ),
                                Pattern.compile(update), Maps.of(
                                        HttpMethods.POST, apartmentHandler::update
                                ),
                                Pattern.compile(delete), Maps.of(
                                        HttpMethods.POST, apartmentHandler::delete
                                )
                        )
                )
                .build();
        int port = 8443;
        try {
            server.serveHTTPS(port);
        } catch (IOException e) {
            log.error("can't serve", e);
        }
    }
}
